class Highlighter
{
    
    constructor(options)
    {   
        this.customClass = {
            REVERT_STYLE : 'revert-text',
            REMOVE_ITEM : 'hidden-text'
        }
        this.items = [];
        this.options = {};
        this.options = options;
        this.options.itemContainer = document.querySelector(this.options.itemContainer);
        this.options.highlighterContainer = document.querySelector(this.options.highlighterContainer);
        this.options.saveBtn = document.querySelector(this.options.saveBtn);
        this.selectItem = {};
        this.selectedItemList = [];
        this.progressItemList = [];
        this.undoClick = false;
        this.content = {
            text : this.options.highlighterContainer.innerText,
            length : this.options.highlighterContainer.innerText.length
        };
        this.itemsRander();
        
        this.options.highlighterContainer.addEventListener('mouseup',this.highlightEvent.bind(this));
        this.options.saveBtn.addEventListener('click',this.saveItems.bind(this));
        if (this.options.redoundo == true) {
            let redoUndoContainer = document.querySelector('.redoundocontainer');
            if (redoUndoContainer) {
                let undoBtn = document.createElement('button');
                undoBtn.classList.add('btn');
                undoBtn.classList.add('redoundbtn');
                undoBtn.innerText = 'Undo';
                undoBtn.addEventListener('click',this.undoRedoEvent.bind({method:'undo', obj:this}))
                redoUndoContainer.appendChild(undoBtn);

                let redoBtn = document.createElement('button');
                redoBtn.classList.add('btn');
                redoBtn.classList.add('redoundbtn');
                redoBtn.innerText = 'Redo';
                redoBtn.addEventListener('click',this.undoRedoEvent.bind({method:'redo', obj:this}))
                redoUndoContainer.appendChild(redoBtn);
            }
        }
    }

    getPreviousSiblings(elem) {
        var sibs = [];
        while (elem = elem.previousSibling) {
            if (elem.nodeType === 3) { 
                sibs.push(elem);
            }
        }
        return sibs;
    }
    undoRedoEvent()
    {
        console.log("process list ", this.obj.progressItemList);
        //return false;
        if (this.obj.progressItemList.length > 0) {
            if (this.method == 'undo') {
                this.obj.undoClick  = true;
                let undoList = this.obj.progressItemList.filter((item)=>{
                    return item.undo == false;
                })
                if (undoList && undoList.length > 0) {
                    
                    var undoItem = undoList[undoList.length - 1];
                    
                    //return false;
                    var undoIndex ;
                    new Promise((resolve, reject)=>{
                        
                        undoIndex = this.obj.progressItemList.findIndex((item)=>{
                            return item.attr.code == undoItem.attr.code;
                        })
                        undoItem = this.obj.progressItemList[undoIndex];
                        undoItem.undo = true;
                        resolve();
                    }).then(()=>{
                        if (undoItem.type == 1) {
                            let selectItem = document.querySelector('[data-code="'+undoItem.attr.code+'"]');
                            selectItem.classList.add(this.obj.customClass.REMOVE_ITEM);

                            let selectText = selectItem.innerText;
                            let newNode = document.createTextNode(selectText);
                            selectItem.parentNode.insertBefore(newNode, selectItem);
                            this.obj.progressItemList[undoIndex]['newNode'] = newNode;
                            console.log("Undo Type 1");
                            this.obj.progressItemList[undoIndex] = undoItem;
                        } 
                        else if (undoItem.type == 2) {
                            let allSelectElement = document.querySelectorAll('[data-parent-code="'+undoItem.attr.prevCode+'"]');
                            var allItemPromise= [];
                            var allText = "";
                            let removeClass = this.obj.customClass.REMOVE_ITEM;
                            [].forEach.call(allSelectElement, function(el) {
                                allItemPromise.push(new Promise((resolve, reject)=>{
                                    allText += el.innerText+" ";
                                    el.classList.add(removeClass);
                                    resolve();
                                }))
                            });
                            Promise.all(allItemPromise).then(()=>{
                                let newSpan = this.obj.wraper(allText, {
                                    dataItem : undoItem.attr.dataItem,
                                    color : undoItem.attr.color,
                                    code  : undoItem.attr.prevCode
                                })
                                $(newSpan).insertBefore(allSelectElement[0]);
                                console.log("Undo Type 2");
                                this.obj.progressItemList[undoIndex] = undoItem;
                            })
                            
                        }
                        else if (undoItem.type == 3) {
                            let selectItem = document.querySelector('[data-code="'+undoItem.attr.code+'"]');
                            selectItem.setAttribute('data-item', undoItem.attr.prevItem.id);
                            selectItem.setAttribute('data-color', undoItem.attr.prevItem.color);
                            selectItem.setAttribute('style', "background-color:"+undoItem.attr.prevItem.color);
                            selectItem.setAttribute('data-code', undoItem.attr.prevCode);
                            console.log("Undo Type 3");
                            this.obj.progressItemList[undoIndex] = undoItem;
                        }
                        else if (undoItem.type == 4) {
                            let selectItem = document.querySelector('[data-code="'+undoItem.attr.code+'"]');
                            var allPromise = [];
                            if (selectItem) {
                                selectItem.classList.remove(this.obj.customClass.REMOVE_ITEM)
                                selectItem.setAttribute('data-code', undoItem.prevCode);
                                 let prevSiblings = this.obj.getPreviousSiblings(selectItem);
                                 var innerText = selectItem.innerText;
                                 undoItem['newNode'] = [];
                                for (let sub of prevSiblings) {
                                    allPromise.push( new Promise((resolve, reject)=>{
                                        if (innerText.endsWith(sub.nodeValue) ) {
                                                
                                                innerText = innerText.slice(0, -sub.data.length);
                                                undoItem['newNode'].push({
                                                    node : sub,
                                                    value  : sub.nodeValue
                                                });
                                                sub.nodeValue = '';
                                        }
                                        resolve();
                                    }) );
                                }
                                Promise.all(allPromise).then(()=>{
                                    console.log(undoItem.newNode);
                                    console.log("Undo Type 4");
                                    this.obj.progressItemList[undoIndex] = undoItem;
                                })
                            }
                            
                        }
                        
                        
                    })
                    
                }
            }
            else if (this.method == 'redo') {
                let redoList = this.obj.progressItemList.filter((item)=>{
                    return item.undo == true;
                })
                if (redoList && redoList.length > 0) {
                    var redoItem = redoList[0];
                    var redoIndex;
                    new Promise((resolve, reject)=>{

                        redoIndex = this.obj.progressItemList.findIndex((item)=>{
                            return item.attr.code == redoItem.attr.code;
                        })
                        redoItem = this.obj.progressItemList[redoIndex];
                        redoItem.undo = false;
                        resolve()
                    }).then(()=>{
                        if (redoItem.type == 1) {
                            let selectItem = document.querySelector('[data-code="'+redoItem.attr.code+'"]');
                            selectItem.classList.remove(this.obj.customClass.REMOVE_ITEM);
                            selectItem.parentNode.removeChild(redoItem.newNode);
                            console.log("Redo Type 1");
                            this.obj.progressItemList[redoIndex] = redoItem;
                        } 
                        else if (redoItem.type == 2) {
                            let selectItem = document.querySelector('[data-code="'+redoItem.attr.prevCode+'"]');
                            let allSelectElement = document.querySelectorAll('[data-parent-code="'+redoItem.attr.prevCode+'"]');
                            var allItemPromise= [];
                            let removeClass = this.obj.customClass.REMOVE_ITEM;
                            [].forEach.call(allSelectElement, function(el) {
                                allItemPromise.push(new Promise((resolve, reject)=>{
                                    el.classList.remove(removeClass);
                                    resolve();
                                }))
                            });
                            Promise.all(allItemPromise).then(()=>{
                                allSelectElement[0].parentNode.removeChild(selectItem);
                                console.log("Redo Type 2");
                                this.obj.progressItemList[redoIndex] = redoItem;
                            })
                            
                        }
                        else if (redoItem.type == 3) {
                            let selectItem = document.querySelector('[data-code="'+redoItem.attr.prevCode+'"]');
                            selectItem.setAttribute('data-item', redoItem.attr.current.id);
                            selectItem.setAttribute('data-color', redoItem.attr.current.color);
                            selectItem.setAttribute('style', "background-color:"+redoItem.attr.current.color);
                            selectItem.setAttribute('data-code', redoItem.attr.code);
                            console.log("Redo Type 3");
                            
                        }
                        else if (redoItem.type == 4) {
                            let selectItem = document.querySelector('[data-code="'+redoItem.prevCode+'"]');
                            var allPromise = [];
                            if (selectItem) {
                                selectItem.classList.add(this.obj.customClass.REMOVE_ITEM)
                                selectItem.setAttribute('data-code', redoItem.attr.code);
                                if (redoItem.newNode && redoItem.newNode.length > 0) {
                                    for(let textItem of redoItem.newNode) {
                                        allPromise.push(new Promise((resolve, reject)=>{
                                            textItem.node.nodeValue = textItem.value;
                                            resolve();
                                        }))
                                    }
                                }
                            }
                            console.log("Redo Type 4");
                            Promise.all(allPromise).then(()=>{
                                this.obj.progressItemList[redoIndex] = redoItem;
                            })
                        }
                        
                    })
                    
                }
            }
        }
    }

    generateHighlightCode()
    {
        var result = '';
        let chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        let length = 8;
        for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
        let checkExist = this.progressItemList.find((item)=>{
            return item.attr.code == result;
        })
        if (checkExist) {
            this.generateHighlightCode();
        }
        return result;
    }

    httpRequest(httpOptions)
    {
        return new Promise((resolve, reject)=>{
            
            const xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                resolve(this.response);
            }
            };
            xhttp.open(httpOptions.method, httpOptions.url, true);
            if (httpOptions.method == 'post') {
                var params = Object.keys(httpOptions.data).map(function(k) {
                    return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
                }).join('&');
                xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            }
            xhttp.send();
            
        })
    }

    itemsRander()
    {
        /* this.httpRequest({
            method : 'get',
            url : 'https://apibinssoft.herokuapp.com'
        }).then((responseData)=>{
            console.log(responseData);
        }) */
        this.items = [
            {
                id : 1,
                name : 'Item 1',
                color : '#DB7093',
                subItems : [
                    {
                        id : 10,
                        name : 'Item 1 - 1',
                        color : '#ba1046',
                    },
                    {
                        id : 11,
                        name : 'Item 1 - 2',
                        color : '#996b79',
                    },
                ]
            },
            {
                id : 2,
                name : 'Item 2',
                color : '#008000',
                subItems : [
                    {
                        id : 12,
                        name : 'Item 2 - 1',
                        color : '#276627',
                    },
                    {
                        id : 13,
                        name : 'Item 2 - 2',
                        color : '#06db06',
                    },
                ]
            },
            {
                id : 3,
                name : 'Item 3',
                color : '#FF6347',
                subItems : []
            },
            {
                id : 4,
                name : 'Item 4',
                color : '#6495ED',
                subItems : []
            },
            {
                id : 5,
                name : 'Item 5',
                color : '#C0C0C0',
                subItems : []
            },
            {
                id : 6,
                name : 'Item 6',
                color : '#F4A460',
                subItems : []
            },
            {
                id : 7,
                name : 'Item 7',
                color : '#008080',
                subItems : []
            },
            {
                id : 8,
                name : 'Item 8',
                color : '#FF6347',
                subItems : []
            },

        ];
        if (this.items.length > 0) {
            for (let item of this.items) {
                let itemName = item.name;
                let itemId  = item.id;
                let itemColor = item.color;

                let span = document.createElement('span');
                span.setAttribute('class', 'selector-item');
                span.setAttribute('data-id', itemId);
                span.setAttribute('data-color', itemColor);
                span.addEventListener('click',this.clickItemEvent.bind({item:item, obj:this}))

                let colorI = document.createElement('i');
                colorI.setAttribute('class','color-icon');
                colorI.setAttribute('style','background-color:'+itemColor);
                span.appendChild(colorI);

                let textEm = document.createElement('em');
                textEm.appendChild(document.createTextNode(itemName));
                span.appendChild(textEm);

                this.options.itemContainer.appendChild( span );
            }
        }
    }
    clickItemEvent(event)
    {
        
        const selectItem = this.item;
        let item = document.querySelector('[data-id="'+selectItem.id+'"]');
        var allItemPromise = [];
        const allSelectElement = document.getElementsByClassName('selector-item');

        [].forEach.call(allSelectElement, function(el) {
            allItemPromise.push(new Promise((resolve, reject)=>{
                el.classList.remove("active");
                resolve();
            }))
            
        });
        Promise.all(allItemPromise).then(()=>{
            item.classList.add('active');
            this.obj.selectItem = selectItem;
        })
        
    }
    clickSubItemEvent()
    {
        const wrapItem = this.wrapItem;
        const newCode = this.obj.generateHighlightCode();
        new Promise((resolve, reject)=>{
            if (this.obj.undoClick == true) {
                this.obj.removePreviousUndoItem();
                this.obj.undoClick = false;    
            }
            this.obj.progressItemList.push({
                type : 3,
                attr : {
                    "prevItem" : {
                        'id':parseInt(wrapItem.getAttribute('data-item')),
                        'color': wrapItem.getAttribute('data-color')
                    },
                    "current" : this.item,
                    "prevCode" : wrapItem.getAttribute('data-code'),
                    "code" : newCode,
                },
                undo : false
            })
            //console.log(this.obj.progressItemList);
            resolve()
        }).then(()=>{
            wrapItem.setAttribute('data-item', this.item.id);
            wrapItem.setAttribute('data-color', this.item.color);
            wrapItem.setAttribute('data-code', newCode);
            wrapItem.setAttribute('style', "background-color:"+this.item.color);
            document.getElementById('subitemmodal').classList.add('hidden');
        })
        

        
        
    }
    highlightEvent()
    {
        var obj = this;
		var sel, range, node; 
		if (Object.keys(obj.selectItem).length >0) {

        
            if (window.getSelection) {
                sel = window.getSelection();
                new Promise((resolve, reject)=>{
                    if(!(/^\s/).test(sel.getRangeAt(0).toString())) {
                    
                        if (!sel.isCollapsed) {
            
                            // Detect if selection is backwards
                            var range = document.createRange();
                            range.setStart(sel.anchorNode, sel.anchorOffset);
                            range.setEnd(sel.focusNode, sel.focusOffset);
                            var backwards = range.collapsed;
                            range.detach();
                
                            // modify() works on the focus of the selection
                            var endNode = sel.focusNode, endOffset = sel.focusOffset;
                            sel.collapse(sel.anchorNode, sel.anchorOffset);
                            
                            var direction = [];
                            if (backwards) {
                                direction = ['backward', 'forward'];
                            } else {
                                direction = ['forward', 'backward'];
                            }
                
                            sel.modify("move", direction[0], "character");
                            sel.modify("move", direction[1], "word");
                            sel.extend(endNode, endOffset);
                            sel.modify("extend", direction[1], "character");
                            sel.modify("extend", direction[0], "word");
                        }
                    }
                    resolve(sel);
                }).then((sel)=>{
                    if (sel.getRangeAt && sel.rangeCount) {
                        range = sel.getRangeAt(0);
                        
                        if(range.startOffset != range.endOffset && range.toString().trim()!=''){
                            
                            var current = {
                                "color" : obj.selectItem.color,
                                "dataItem" : obj.selectItem.id,
                                "code" : obj.generateHighlightCode()
                            }
                            //console.log(range);
                            let newCreatedItem = obj.wraper(range,current);
                           
                            if(range.commonAncestorContainer.nodeName == 'SPAN') {
                                this.splitNode(range.commonAncestorContainer);
                            } else { // only for main item select and not spliting node
                                if (obj.undoClick == true) {
                                    //this.progressItemList = [];
                                    this.removePreviousUndoItem();
                                    this.undoClick = false;    
                                }
                                this.progressItemList.push({
                                    type : 1,
                                    attr : current,
                                    undo : false
                                })
                                
                            }
                            
                        }
                    }
                    obj.clearSelection();
                    
                })

                
            } else if (document.selection && document.selection.createRange) {
                range = document.selection.createRange();
                range.collapse(false);
                range.pasteHTML(html);
                obj.clearSelection();
            }
        } else {
            obj.clearSelection();
        }
    }
    clearSelection()
	{
	 if (window.getSelection) {window.getSelection().removeAllRanges();}
	 else if (document.selection) {document.selection.empty();}
    }
    splitNode(node) {
        let iEle = node.getElementsByTagName('i');
        node.removeChild( iEle[iEle.length-1] );
        var nodeInnerHtml = node.innerHTML;
        var leftPart = nodeInnerHtml.substr(0, nodeInnerHtml.indexOf('<span'));
        var childHtml = node.getElementsByTagName('span')[0];
        var rightPart = nodeInnerHtml.substr(nodeInnerHtml.indexOf('</span>')+7);
        var parentCode = node.getAttribute('data-code');
        var current = {
            "color" : node.getAttribute('data-color'),
            "dataItem" : node.getAttribute('data-item'),
            "prevCode" : node.getAttribute('data-code'),
            "code" : parentCode
        }
        if (this.undoClick == true) {
            this.removePreviousUndoItem();
            this.undoClick = false;    
        }
        this.progressItemList.push({
            type : 2,
            attr : current,
            undo : false
        })
        //console.log(this.progressItemList);

        var div = document.createElement('div');
        if (leftPart != ''){
            current.code = this.generateHighlightCode();
            let leftNode = this.wraper(leftPart,current);
            leftNode.setAttribute('data-parent-code', parentCode);
            div.appendChild(leftNode);
        }
        current.code = this.generateHighlightCode();
        childHtml.setAttribute('data-code', current.code);
        childHtml.setAttribute('data-parent-code', parentCode);
        div.appendChild(childHtml)
        if (rightPart != ''){
            current.code = this.generateHighlightCode();
            let rightNode = this.wraper(rightPart,current);
            rightNode.setAttribute('data-parent-code', parentCode);
            div.appendChild(rightNode);
        }
        node.outerHTML = div.innerHTML;
        
        let elementsArray = this.options.highlighterContainer.querySelectorAll('span.select-text');
        
        let classObj = this;
        elementsArray.forEach(function(elem) {
            let clickItem = {               
                color : elem.getAttribute('data-color'),
                dataItem : elem.getAttribute('data-item')
            }
            elem.querySelector('i.cross').addEventListener('click',classObj.removeItem.bind(classObj));
            elem.addEventListener('click', classObj.addSubItem.bind({item:clickItem,wrapItem: elem, obj:classObj}));
            
        });
        
      }
    wraper(range,current)
	{
		var wrapItem = document.createElement("span");
        wrapItem.setAttribute('style',"background-color:"+current.color);
        wrapItem.classList.add('select-text')
        wrapItem.setAttribute('data-item',current.dataItem);
        wrapItem.setAttribute('data-color',current.color);
        wrapItem.setAttribute('data-code',current.code);
    	if(typeof range == 'object'){
	    	range.surroundContents(wrapItem);
	    } else {
	    	wrapItem.innerHTML = range;
        }
        wrapItem.addEventListener('click', this.addSubItem.bind({ wrapItem: wrapItem,  obj:this}));
    	var cross = document.createElement('i');
        cross.setAttribute('class',"cross cross-icon");
        cross.addEventListener('click', this.removeItem.bind(this))
    	wrapItem.appendChild(cross);
    	return wrapItem;
    }
    addSubItem()
    {
        
        const wrapItem = this.wrapItem;
        var clickedItem = wrapItem.getAttribute('data-item');
        var subItems = this.obj.items.find((i)=>{
            return i.id == parseInt(clickedItem)
        })
        if(subItems) {
            subItems = subItems.subItems;
            const modelContainer = document.getElementById('subitemmodal');
            modelContainer.classList.remove('hidden');
            var container = modelContainer.getElementsByClassName('modal-custom-container')[0];
            container = container.getElementsByClassName('modal-content')[0];
            container.innerHTML = '';
            if (subItems.length > 0) {
                
                
                for(let item of subItems) {
                    var itemDiv = document.createElement('div');
                    itemDiv.classList.add('sub-item-container');
        
                    let span = document.createElement('span');
                    span.setAttribute('class', 'selector-item');
                    span.setAttribute('data-id', item.id);
                    span.setAttribute('data-color', item.color);
                    
                    span.addEventListener('click',this.obj.clickSubItemEvent.bind({item:item, wrapItem: wrapItem,  obj:this.obj}))
        
                    let colorI = document.createElement('i');
                    colorI.setAttribute('class','color-icon');
                    colorI.setAttribute('style','background-color:'+item.color);
                    span.appendChild(colorI);

                    let textEm = document.createElement("em");
                    textEm.appendChild(document.createTextNode(item.name));
                    span.appendChild(textEm);
        
                    itemDiv.appendChild(span);
                    container.appendChild(itemDiv);
                }
            } else {
                let itemDiv = document.createElement('div');
                itemDiv.classList.add('no-item-exists');
                itemDiv.appendChild(document.createTextNode('No Sub Item exist'));
                container.appendChild(itemDiv);
            }
            
            document.getElementsByClassName('close-modal')[0].addEventListener('click',()=>{
                modelContainer.classList.add('hidden');
            })
        }
    }
    removeItem() {

        var selectItem = event.target.parentElement;
        selectItem.classList.add(this.customClass.REMOVE_ITEM);
        let newNode = document.createTextNode(selectItem.innerText);
        
        selectItem.parentNode.insertBefore(newNode, selectItem);
        let newCode = this.generateHighlightCode();
        if (this.undoClick == true) {
            this.removePreviousUndoItem();
            this.undoClick = false;    
        }
        this.progressItemList.push({
            type : 4,
            attr : {
                code : newCode
            },
            newnode : newNode,
            prevCode : selectItem.getAttribute('data-code'),
            undo : false
        })
        selectItem.setAttribute('data-code', newCode);
        //console.log(this.progressItemList);
        /* if (wrap){
            var wrap = event.target.parentElement;
            $(wrap).addClass(this.customClass.REMOVE_ITEM);
            let newCode = this.generateHighlightCode();
            let newNode = document.createTextNode($(wrap).text());
            $(newNode).insertBefore($(wrap));
            this.progressItemList.push({
                type : 4,
                attr : {
                    prevCode : $(wrap).attr('data-code'),
                    code : newCode
                },
                newnode : newNode,
                undo : false
            })
            console.log(this.progressItemList);
            
        } */
		event.stopPropagation();
	}
    
    getSelectedItems(){
        this.selectedItemList =[];
		var childNodes = this.options.highlighterContainer.childNodes;
        var startPoint = 0;
        var allItemHighlightPromise = [];
        var allremovePromise = [];
        [].forEach.call(document.querySelectorAll('.'+this.customClass.REMOVE_ITEM),function(e){
            allremovePromise.push(new Promise((resolve, reject)=>{
                e.parentNode.removeChild(e);
                resolve();
            }))
        });
        return Promise.all(allremovePromise).then(()=>{
            
            for(let child of childNodes) {
                allItemHighlightPromise.push(new Promise((resolve, reject)=>{
                    let row = {};
                    startPoint += (child.innerText)? child.innerText.length : child.length;
                    
                    if(child.nodeName == 'SPAN') {
                        let start = (startPoint - child.innerText.length);
                        if(/^\s/.test(child.innerText)) {
                            start += 1;
                        }
                        let selectText = child.innerText.trim();
                        
                        row = {
                            text : selectText,
                            item : parseInt(child.getAttribute('data-item')),
                            start :  start,
                            length : selectText.length
                        }
                    }
                    resolve(row);
                }));
            }
        }).then(()=>{
            return Promise.all(allItemHighlightPromise);
        })
        
    }
    removePreviousUndoItem()
    {
        let filterData = this.progressItemList.filter((i)=>{
            return i.undo == false;
        })
        this.progressItemList = filterData;
    }
    
    saveItems()
    {
        this.getSelectedItems().then((highlightResult)=>{
            highlightResult = highlightResult.filter(value => Object.keys(value).length !== 0);
            console.log(highlightResult);
        });
    }
}

window.onload = ()=>{
    const options = {
        'itemContainer' : "#itemContainer",
        'highlighterContainer' : "#highlighterContainer",
        'saveBtn': '#saveItemBtn',
        'redoundo' : true
    }
    const highlighterObj = new Highlighter(options);
}